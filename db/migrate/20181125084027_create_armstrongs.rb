# frozen_string_literal: true

class CreateArmstrongs < ActiveRecord::Migration[5.2]
  def change
    create_table :armstrongs do |t|
      t.integer :digits
      t.text :numbers

      t.timestamps
    end
  end
end
