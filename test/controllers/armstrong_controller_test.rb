# frozen_string_literal: true

require 'test_helper'

class ArmstrongControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get armstrong_path
    assert_response 301
  end

  test 'should get input' do
    get armstrong_input_url
    assert_response :success
  end

  test 'should get result' do
    get result_armstrong_path
    assert_response 302
  end
end
