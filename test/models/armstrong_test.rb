# frozen_string_literal: true

require 'test_helper'

class ArmstrongTest < ActiveSupport::TestCase
  test 'adding data' do
    num = Armstrong.new digits: 1, numbers: '1, 2 ,3'
    num.save
    assert_not Armstrong.find_by(digits: 1).nil?
  end

  test 'duplications' do
    num = Armstrong.new digits: 3, numbers: '1, 2 ,3'
    res = num.save
    assert res
    num = Armstrong.new digits: 3, numbers: '1, 2 ,3'
    res = num.save
    assert_not res
  end
end
