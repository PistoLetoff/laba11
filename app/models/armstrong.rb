# frozen_string_literal: true

class Armstrong < ApplicationRecord
  validates :digits, uniqueness: true
end
