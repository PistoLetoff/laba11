# frozen_string_literal: true

class ArmstrongController < ApplicationController
  def input; end

  def result
    @n = params[:number].to_i
    if @n < 1
      flash[:alert] = "There are't numbers with #{@n} digits"
      redirect_to armstrong_path
      return
    end

    if @n > 39
      @result = []
      return
    end

    if @n > 10
      flash[:alert] = 'Very big number of digits. My computer will brake =('
      redirect_to armstrong_path
      return
    end

    res = Armstrong.find_by(digits: @n)
    if !res.nil?
      @result = ActiveSupport::JSON.decode(res.numbers)
    else
      @result = (10**(@n - 1)).upto(10**@n - 1).select do |num|
        num.digits.reduce(0) { |sum, dig| sum + dig**@n }.equal? num
      end

      unless @result.empty?
        res = Armstrong.new digits: @n, numbers: ActiveSupport::JSON.encode(@result)
        res.save
      end
    end
  end

  def inspect_db
    respond_to do |format|
      @numbers = Armstrong.all
      format.xml { render xml: @numbers }
      format.json { render json: @numbers }
      format.html {}
    end
  end
end
