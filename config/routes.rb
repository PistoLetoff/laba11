# frozen_string_literal: true

Rails.application.routes.draw do
  get 'armstrong/input'
  get 'armstrong', to: redirect('armstrong/input')
  get 'armstrong/result', as: :result_armstrong
  get 'armstrong/inspect_db'

  root 'armstrong#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
